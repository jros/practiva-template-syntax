import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { ButtonsModule } from "ngx-bootstrap";

import { AppComponent } from "./app.component";

@NgModule({
    imports: [
        BrowserModule,
        ButtonsModule.forRoot()
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
